/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/09 15:02:29 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/09 16:19:15 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"

static t_file	*set_workspace(t_file **head, int fd)
{
	t_file	*tmp;

	if (fd < 0)
		return (NULL);
	tmp = *head;
	while (tmp)
		if (tmp->fd == fd)
			return (tmp);
		else
			tmp = tmp->next;
	tmp = (t_file*)malloc(sizeof(t_file));
	if (tmp == NULL)
		return (NULL);
	tmp->reminder = NULL;
	tmp->fd = fd;
	tmp->lp = NULL;
	tmp->next = *head;
	*head = tmp;
	return (tmp);
}

static void		del_workspace(t_file **head, int fd)
{
	t_file	*prev;
	t_file	*tmp;

	prev = NULL;
	tmp = *head;
	while (tmp->fd != fd)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if (prev)
	{
		prev->next = tmp->next;
		free(tmp->reminder);
		free(tmp);
		return ;
	}
	prev = tmp->next;
	free(tmp->reminder);
	free(tmp);
	*head = prev;
	return ;
}

static int		create_line(t_file *p, char *lf, char **line)
{
	size_t	len;
	char	*tmp;

	tmp = p->lp ? p->lp : p->reminder;
	len = lf - tmp;
	*line = (char*)malloc((len + 1) * sizeof(char));
	if (*line == NULL)
		return (-1);
	*((*line) + len) = '\0';
	ft_memcpy(*line, tmp, len);
	if (*lf && *(lf + 1))
		p->lp = lf + 1;
	else
	{
		free(p->reminder);
		p->reminder = NULL;
		p->lp = NULL;
	}
	return (1);
}

static int		exit_event_handler(ssize_t nbytes, t_file *p,
								t_file **head, char **line)
{
	char	*tmp;

	tmp = p->lp ? p->lp : p->reminder;
	if (p->reminder)
		return (create_line(p, ft_strchr(tmp, '\0'), line));
	del_workspace(head, p->fd);
	return (int)(nbytes);
}

int				get_next_line(const int fd, char **line)
{
	static t_file	*head;
	t_file			*p;
	char			buf[BUFF_SIZE + 1];
	char			*lf;
	ssize_t			nbytes;

	p = set_workspace(&head, fd);
	if (p == NULL || line == NULL)
		return (-1);
	lf = NULL;
	if (p->lp)
		lf = ft_strchr(p->lp, '\n');
	while (lf == NULL)
	{
		nbytes = read(fd, buf, BUFF_SIZE);
		if (nbytes == -1 || nbytes == 0)
			return (exit_event_handler(nbytes, p, &head, line));
		buf[nbytes] = '\0';
		lf = ft_strjoin(p->reminder, buf);
		p->lp = p->lp - p->reminder + lf;
		free(p->reminder);
		p->reminder = lf;
		lf = ft_strchr(p->lp ? p->lp : p->reminder, '\n');
	}
	return (create_line(p, lf, line));
}
