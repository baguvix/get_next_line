#include "libft.h"
#include <stdio.h>
#include <fcntl.h>
#include "get_next_line.h"

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		printf("argv");
		return 0;
	}

	char *str = 0;
	int retv = 0;
	// for (size_t i = 0; i < 2500; i++)
	// {
		int fd = open(argv[1], O_RDONLY);

		while ((retv = get_next_line(fd, &str)) > 0)
		{
			printf("%s\n", str);
			free(str);
		}
		close(fd);
	// }
	return 0;
}
